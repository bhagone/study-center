# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
from itemadapter import ItemAdapter
import pandas as pd

class StudycenterPipeline:
    def process_item(self, item, spider):
        return item
    def close_spider(self, spider):
        if spider.generatepptx:
            df = pd.DataFrame(columns= ["Course Name", "Category", "Sub Category", "Course Website", "Duration", "Duration Term", "Study Mode", "Degree Level", "Monthly Intake", "Intake Day", "Intake Month", "Apply Day", "Apply Month", "City", "Domestic only", "International Fee", "Domestic fee", "Fee Term", "Fee Year", "Currency", "Study Load", "IELTS Listening", "IELTS Speaking", "IELTS Writing", "IELTS Reading", "IELTS Overall", "PTE Listening", "PTE Speaking", "PTE Writing", "PTE Reading", "PTE Overall", "TOFEL Listening", "TOFEL Speaking", "TOFEL Writing", "TOFEL Reading", "TOFEL Overall", "English Test", "Listening", "Speaking", "Writing", "Overall", "Academic Level", "Academic Score", "Score Type", "Academic Country", "Other Test", "Score", "Other Requirement", "Course Description", "Course Structure", "Career", "Scholarship"])
            
            df['Course Name'] = spider.course_name
            df['Category'] = spider.category
            df['Course Website'] = spider.course_website
            df['Study Mode'] = spider.study_mode
            df['Study Load'] = spider.study_load
            df['Degree Level'] = spider.degree
            df['Duration'] = spider.duration
            df['Duration Term'] = spider.duration_term
            df['City'] = spider.city
            df['Intake Month'] = spider.intake_month
            df['Domestic only'] = spider.dom_only
            df['International Fee'] = spider.fee_int
            df['Domestic fee'] = spider.fee_dom
            df['Fee Year'] = spider.fee_year
            df['Fee Term'] = spider.fee_term
            df['Currency'] = spider.currency

            df['IELTS Overall'] = spider.ielts_overall
            df['IELTS Reading'] = spider.ielts_reading
            df['IELTS Writing'] = spider.ielts_writing
            df['IELTS Listening'] = spider.ielts_listening
            df['IELTS Speaking'] = spider.ielts_speaking

            df['TOFEL Overall'] = spider.toefl_overall
            df['TOFEL Writing'] = spider.toefl_writing
            df['TOFEL Speaking'] = spider.toefl_speaking
            df['TOFEL Listening'] = spider.toefl_listening
            df['TOFEL Reading'] = spider.toefl_reading

            df['PTE Overall'] = spider.pte_overall
            df['PTE Reading'] = spider.pte_reading
            df['PTE Listening'] = spider.pte_listening
            df['PTE Writing'] = spider.pte_writing
            df['PTE Speaking'] = spider.pte_speaking

            df['Course Description'] = spider.course_des
            df['Course Structure'] = spider.course_struct
            df['Career'] = spider.career
            df.to_excel(spider.file_name + ".xlsx")
