
import scrapy
import json
from scrapy.http import JsonRequest
import time
from bs4 import BeautifulSoup
import os.path
import re
import numpy as np
import pandas as pd
class YorkSpider_old(scrapy.Spider):
    name = "york_old"
    token = ""
    data_directory = ""
    course_name = []
    category = []
    course_website = []
    duration = []
    duration_term = []
    degree = []
    intake_month = []
    apply_month = []
    city = []
    study_mode = []
    dom_only = []
    prerequisites = []
    fee_dom = []
    fee_int = []
    fee_year = []
    fee_term = []
    currency = []
    study_load = []

    ielts_overall = []
    ielts_reading = []
    ielts_writing = []
    ielts_listening = []
    ielts_speaking = []
    toefl_overall = []
    toefl_reading = []
    toefl_writing = []
    toefl_listening = []
    toefl_speaking = []
    pte_overall = []
    pte_reading = []
    pte_writing = []
    pte_listening = []
    pte_speaking = []
    course_struct = []
    career = []
    course_des = []
    def start_requests(self):
        current_directory = os.getcwd()
        self.data_directory = os.path.join(current_directory, r'data_JCU')
        if not os.path.exists(self.data_directory):
            os.makedirs(self.data_directory)

        if 1 == 0: 
            x = range(0,29)
            urls = []
            for n in x:
                url = "https://www.yorksj.ac.uk/courses/?hideTabs=true&tab=courses&profile=_default&sort=metacourseTitle&collection=yorksj-meta&query=!showall&start_rank=" + str((n*10) + 1)
                urls.append(url)
            for url in urls:
                yield scrapy.Request(url=url, callback=self.parse_courses_metadata)
        else:
            urls = [
                "https://www.yorksj.ac.uk/ot-msc/"
            ]
            for url in urls:
                yield scrapy.Request(url=url, callback=self.parse_courses)
    def parse_courses_metadata(self, response):
        all = response.xpath('//a//@title').getall()
        filter_all = list(filter(lambda x: '/courses/undergraduate/' in x, all))
        filter_all_post = list(filter(lambda x: '/courses/postgraduate/' in x, all))
        for url in filter_all:
            yield scrapy.Request(url=url, callback=self.parse_courses)
        for url in filter_all_post:
            yield scrapy.Request(url=url, callback=self.parse_courses)
    
    def parse_courses(self, response):
        # body_soup = BeautifulSoup(response.text, 'html.parser')
        
        name = response.xpath('//h1/span/text()').get().strip()
        overview = ' '.join(response.xpath('//h2[contains(text(), "Course overview")]/following-sibling::*/descendant::p/text()').extract())
        #category
        duration_elements  = response.xpath('//li/strong[contains(text(), "Duration")]/parent::*/text()').extract()
        duration = duration_elements[1] if duration_elements else ''
        duration = duration.replace("–", "").strip()
        duration = duration.replace("One year", "1 year")
        duration = duration.replace("One Year", "1 year")
        duration = duration.replace("Two year", "2 years")
        duration = duration.replace("Two Year", "2 years")
        duration_extract = re.findall(r'\b\d+\b',duration)
        duration_value = duration_extract[0] if duration_extract else ''
        study_load = ""
        duration_term = ''
        if "year" in duration:
            duration_term = "Years"
        elif "month" in duration:
            duration_term = "Months"
        
        is_full = "full-time" in duration or "full time" in duration or "Full-time" in duration or "Full-time" in duration
        is_part = "part time" in duration or "part-time" in duration or "Part time" in duration or "Part-time" in duration
        if is_full and is_part:
            study_load = "Both"
        if is_part:
            study_load = "Part time"
        if is_full:
            study_load = "Full time"

        cate = response.xpath('//li/strong[contains(text(), "School")]/parent::*/text()').extract()
        category_name = cate[1] if cate else ''
        category_name = category_name.replace("–", "").strip()
        print(category_name)
        international_fee = ''
        home_fee = ''
        try:
            home_fee_message_element = response.xpath("//span[contains(text(), 'UK and EU')]/text()").get()
            home_fee_message = response.xpath("//span[contains(text(), 'UK and EU')]/following-sibling::*/text()").get()
            if home_fee_message:
                home_fee_list = re.findall(r'[\$¢£]\b\d+\b', (home_fee_message.strip().replace(',','')))
                home_fee = home_fee_list[0] if home_fee_list else ''
            
        except:
            home_fee_message_element = response.xpath("//div/h3[contains(text(), 'UK and EU')]/text()").get()
            home_fee_message = response.xpath("//div/h3[contains(text(), 'UK & EU')]/following-sibling::p").get()
            home_fee_list = re.findall(r'[\$¢£]\b\d+\b', (home_fee_message.replace(',','')))
            home_fee = home_fee_list[0] if home_fee_list else ''

        try:
            international_fee_message_element = response.xpath("//span[contains(text(), 'International')]/text()").get()
            international_fee_message = response.xpath("//span[contains(text(), 'International')]/following-sibling::*/text()").get()
            if international_fee_message:
                international_fee_list = re.findall(r'[\$¢£]\b\d+\b', (international_fee_message.replace(',','')))
                international_fee = international_fee_list[0] if international_fee_list else ''
        except:
            international_fee_message_element = response.xpath("//div/h3[contains(text(), 'International')]/text()").get()
            international_fee_message = response.xpath("//div/h3[contains(text(), 'International (non-EU)')]/following-sibling::p").get()
            international_fee_list = re.findall(r'[\$¢£]\b\d+\b', (international_fee_message.replace(',','')))
            international_fee = international_fee_list[0] if international_fee_list else ''
        
        fee_term = ''
        if '2020-21' in international_fee_message_element:
            fee_term = "2021"
        elif '2020-21' in home_fee_message_element:
            fee_term = "2021"
        elif '2019-20' in international_fee_message_element:
            fee_term = "2020"
        elif '2019-20' in home_fee_message_element:
            fee_term = "2020"
        location =  response.xpath('//title[contains(text(), "Course location")]/text()/parent::*/parent::*/following-sibling::*/text()').get()
        print(location)
        study_mode = ""
        
        if "Online" in location or "Offsite" in location:
            study_mode = "Online"
        else:
            study_mode = "On campus"
        course =  response.xpath('//p[contains(@class, "h-alt")]/text()').get().strip()
        if "Postgraduate" in course or "Masters" in course or "MA" in course:
            course = "Postgraduate"
        
        if "Undergraduate" in course:
            course = "Undergraduate"

       
        if "Postgraduate" in course or "Masters" in course or "MA" in course:
            course = "Postgraduate"
        
        if "Undergraduate" in course:
            course = "Undergraduate"
        

        # Start Month
        intake_month = ''
        start_month = response.xpath('//ul/li/strong[contains(text(), "Start date")]/parent::*').get()
        if start_month:
            intake_month = ','.join(np.unique(re.findall(r'(?:Janary|February|Marcg|April|May|June|July|Augest|September|October|November|December)', (start_month))))

        #start module
        course_desc = ' '.join(response.xpath('//h3[contains(text(), "Modules")]/following-sibling::*/descendant::p/text()').extract())

        #start IELTS
        modules_element_ielts = ' '.join(response.xpath('//p[contains(text(), "IELTS level")]/text()').extract())
        ielts_list = re.findall(r'(\d+\.\d+?)|\.\d+', (modules_element_ielts))
        if len(ielts_list) == 0:
            modules_element_ielts = ' '.join(response.xpath('//p[contains(text(), "IELTS")]/text()').extract())
            ielts_list = re.findall(r'\b\d+\b', (modules_element_ielts))
        if len(ielts_list) == 0:
            ielts_list= ["6.0", "5.5"]
        print(ielts_list)


        career_outcomes = ' '.join(response.xpath('//h2[contains(text(), "Career outcomes")]/following-sibling::*/descendant::p/text()').extract())
        
        self.course_name.append(name)
        self.category.append(category_name)
        self.course_website.append(response.url)
        self.duration.append(duration_value)
        self.duration_term.append(duration_term)

        self.city.append(location)
        self.degree.append(course)
        self.intake_month.append(intake_month)
        self.apply_month.append("")

        self.study_mode.append(study_mode)
        
        self.fee_dom.append(home_fee.replace('£',''))
        self.fee_int.append(international_fee.replace('£',''))

        self.fee_year.append("Year")
        self.fee_term.append(fee_term)
        self.currency.append("GBP")
        self.study_load.append(study_load)
        over_all = ielts_list[0] if ielts_list else ''
        ielts_len = len(ielts_list)
        print(ielts_len)
        if not over_all and not international_fee and "International students" not in duration:
            self.dom_only.append("True")
        else:
            self.dom_only.append("False")
        if ielts_len == 1:
            self.ielts_overall.append(over_all)
            self.ielts_reading.append("")
            self.ielts_writing.append("")
            self.ielts_listening.append("")
            self.ielts_speaking.append("")
        elif ielts_len == 2:
            ielts_other = ielts_list[1] if ielts_list else ''
            self.ielts_overall.append(over_all)
            self.ielts_reading.append(ielts_other)
            self.ielts_writing.append(ielts_other)
            self.ielts_listening.append(ielts_other)
            self.ielts_speaking.append(ielts_other)
        else:
            self.ielts_overall.append("")
            self.ielts_reading.append("")
            self.ielts_writing.append("")
            self.ielts_listening.append("")
            self.ielts_speaking.append("")

        self.toefl_overall.append("")
        self.toefl_reading.append("")
        self.toefl_writing.append("")
        self.toefl_listening.append("")
        self.toefl_speaking.append("")

        self.pte_overall.append("")
        self.pte_reading.append("")
        self.pte_writing.append("")
        self.pte_listening.append("")
        self.pte_speaking.append("")

        
        self.course_struct.append(course_desc)
        self.career.append(career_outcomes)
        self.course_des.append(overview)


