
import scrapy
import json
from scrapy.http import JsonRequest
import time
from bs4 import BeautifulSoup
import os.path
import re
import numpy as np
import pandas as pd
class EdSpider(scrapy.Spider):
    name = "york"
    file_name = 'York St John University'
    token = ""
    data_directory = ""
    course_name = []
    category = []
    course_website = []
    duration = []
    duration_term = []
    degree = []
    intake_month = []
    apply_month = []
    city = []
    study_mode = []
    dom_only = []
    prerequisites = []
    fee_dom = []
    fee_int = []
    fee_year = []
    fee_term = []
    currency = []
    study_load = []

    ielts_overall = []
    ielts_reading = []
    ielts_writing = []
    ielts_listening = []
    ielts_speaking = []
    toefl_overall = []
    toefl_reading = []
    toefl_writing = []
    toefl_listening = []
    toefl_speaking = []
    pte_overall = []
    pte_reading = []
    pte_writing = []
    pte_listening = []
    pte_speaking = []
    course_struct = []
    career = []
    course_des = []
    index = 1
    generatepptx = True
    def start_requests(self):
        x = range(0,29)
        urls = []
        for n in x:
            url = "https://www.yorksj.ac.uk/courses/?hideTabs=true&tab=courses&profile=_default&sort=metacourseTitle&collection=yorksj-meta&query=!showall&start_rank=" + str((n*10) + 1)
            urls.append(url)
        for url in urls:
            yield scrapy.Request(url=url, callback=self.pare_course_list)
    
    def pare_course_list(self, response):
        all = response.xpath('//div//li[contains(@class, "result-type--course")]')
        for item in all:
            overview = item.xpath(".//div[contains(@class,'fb-result__summary')]/text()").get()
            title = item.xpath(".//div[contains(@class,'fb-result__intro')]/h2/a/text()").get()
            tags = item.xpath(".//ul[contains(@class,'fb-taglist')]/li/text()").extract()
            if len(tags) >= 3:
                level = tags[0]
                if level:
                    level = level.replace('Taught', '')
                    level = level.replace('Research', '')
                    level = level.replace('Module', '')
                    level = level.strip()
                    if 'Undergraduate' in level or 'Postgraduate' in level:
                        months = []
                        for month in tags[1].split("|"):
                            month = month.replace('2020', '')
                            month = month.replace('2021', '')
                            months.append(month.strip())
                        month = ','.join(list(dict.fromkeys(months)))
                        campus = tags[2]
                        meta_data = {
                            'level': level,
                            'overview': overview.strip(),
                            'title': title,
                            'month': month,
                            'campus': campus
                        }
                        url = item.xpath(".//div[contains(@class,'fb-result__intro')]/h2/a//@title").get()
                        yield scrapy.Request(url=url, callback=self.parese_course, meta=meta_data)

        #for item in all:
        #    url = item.xpath(".//@href").get()
        #    url = "https://www.ed.ac.uk" + url
        #    yield scrapy.Request(url=url, callback=self.parese_course)
        #custom-link

    def parese_course(self, response):
        basic_meta = response.meta
        if basic_meta:
            name = basic_meta['title']
            category = self.get_category(response)
            if category:
                course_data = {
                    'name': name,
                    'category': category,
                    'url': response.url,
                    'duration': self.get_duration(response),
                    'duration_term': self.get_duration_term(response),
                    'study_mode': self.get_study_mode(response),
                    'study_load': self.get_study_load(response),
                    'degree_level': basic_meta['level'],
                    'intake_month': basic_meta['month'],
                    'city': basic_meta['campus'],
                    'ielts': self.get_ielts(response),
                    'toefl': self.get_toefl(response),
                    'english': self.get_english(response),
                    'overview':basic_meta['overview'],
                    'career': self.get_career(response),
                    'course_structure': self.get_course_structure(response),
                    'dom_fee':self.get_dom_fee(response),
                    'int_fee':self.get_int_fee(response),
                    'fee_term': self.get_fee_term(response),
                    'fee_year': self.get_fee_year(response),
                }
                self.set_data(course_data)
            else:
                self.index = self.index + 1
                #print(response.url)
                #print(self.index)
    def set_data(self, course_data):
        self.course_name.append(course_data['name'])
        self.category.append(course_data['category'])
        self.course_website.append(course_data['url'])
        self.duration.append(course_data['duration'])
        self.duration_term.append(course_data['duration_term'])
        self.study_mode.append(course_data['study_mode'])
        self.study_load.append(course_data['study_load'])
        self.degree.append(course_data['degree_level'])
        self.intake_month.append(course_data['intake_month'])
        self.city.append(course_data['city'])
        self.fee_dom.append(course_data['dom_fee'])
        self.fee_int.append(course_data['int_fee'])
        self.fee_year.append(course_data['fee_year'])
        self.fee_term.append(course_data['fee_term'])
        self.currency.append("GBP")
        self.apply_month.append("")
        ielts_all = course_data['ielts']
        if not ielts_all[0] and not course_data['int_fee']:
            self.dom_only.append("True")
        else:
            self.dom_only.append("False")
        
        self.ielts_overall.append(ielts_all[0])
        self.ielts_reading.append(ielts_all[1])
        self.ielts_writing.append(ielts_all[2])
        self.ielts_listening.append(ielts_all[3])
        self.ielts_speaking.append(ielts_all[4])

        #toefl_all = course_data['toefl']
        self.toefl_overall.append('')
        self.toefl_reading.append('')
        self.toefl_writing.append('')
        self.toefl_listening.append('')
        self.toefl_speaking.append('')

        self.pte_overall.append("")
        self.pte_reading.append("")
        self.pte_writing.append("")
        self.pte_listening.append("")
        self.pte_speaking.append("")

        
        self.course_struct.append(course_data['course_structure'])
        self.career.append(course_data['career'])
        self.course_des.append(course_data['overview'])
    def get_name(self, response):
        name = response.xpath("//h1[contains(@class, 'page-header')]/text()").get()
        if name:
            return name.strip()
        return ''
    def get_fee_year(self, response):
        #fee_dom_2021 = response.xpath("//span[contains(text(), 'UK and EU 2020-21')]/following-sibling::*")
        fee_term = response.xpath('//h3[contains(text(), "Tuition Fees")]/following-sibling::*/p[1]//span[contains(text(), "20")]/text()').get().strip()
        if fee_term:
            if '2020-21' in fee_term:
                return "2021"
            if '2019-20' in fee_term:
                return "2019"
        return ''
    def get_fee_term(self, response):
        #fee_dom_2021 = response.xpath("//span[contains(text(), 'UK and EU 2020-21')]/following-sibling::*")
        fee_term = response.xpath('//h3[contains(text(), "Tuition Fees")]/following-sibling::*/p[1]//span[contains(text(), "per")]/text()').get().strip()
        if fee_term:
            if 'year' in fee_term:
                return "Year"
        return ''
    def get_dom_fee(self, response):
        #fee_dom_2021 = response.xpath("//span[contains(text(), 'UK and EU 2020-21')]/following-sibling::*")
        fee_dom_2021 = response.xpath('//h3[contains(text(), "Tuition Fees")]/following-sibling::*/p[1]//span[contains(text(), "£")]/text()').get().strip()
        if fee_dom_2021:
            return self.get_currency(fee_dom_2021)
        return ''
    def get_int_fee(self, response):
        #fee_dom_2021 = response.xpath("//span[contains(text(), 'UK and EU 2020-21')]/following-sibling::*")
        fee_dom_2021 = response.xpath('//h3[contains(text(), "Tuition Fees")]/following-sibling::*/p[2]//span[contains(text(), "£")]/text()').get().strip()
        if fee_dom_2021:
            return self.get_currency(fee_dom_2021)
        return ''
    def get_category(self, response):
        category = response.xpath('//li/strong[contains(text(), "School")]/parent::*/text()').extract()
        category_name = category[1] if category else ''
        category_name = category_name.replace("–", "").strip()
        return category_name
    def get_duration(self, response):
        duration_elements  = response.xpath('//li/strong[contains(text(), "Duration")]/parent::*/text()').extract()
        duration = duration_elements[1] if duration_elements else ''
        duration = duration.replace("–", "").strip()
        duration = duration.replace("One year", "1 year")
        duration = duration.replace("One Year", "1 year")
        duration = duration.replace("Two year", "2 years")
        duration = duration.replace("Two Year", "2 years")
        duration_extract = re.findall(r'\b\d+\b',duration)
        duration_value = duration_extract[0] if duration_extract else ''
        return duration_value
    def get_duration_term(self, response):
        duration_elements  = response.xpath('//li/strong[contains(text(), "Duration")]/parent::*/text()').extract()
        duration = duration_elements[1] if duration_elements else ''
        duration = duration.replace("–", "").strip()
        duration = duration.replace("One year", "1 year")
        duration = duration.replace("One Year", "1 year")
        duration = duration.replace("Two year", "2 years")
        duration = duration.replace("Two Year", "2 years")
        if 'Year' in duration or 'year' in duration:
            return "Years"
        elif 'Month' in duration or 'month' in duration:
            return "Months"
        elif 'Week' in duration or 'week' in duration:
            return "Weeks"
        else:
            return ""
    def get_study_mode(self, response):
        location =  response.xpath('//title[contains(text(), "Course location")]/text()/parent::*/parent::*/following-sibling::*/text()').get()
        if "Online" in location or "Offsite" in location:
            return "Online"
        else:
            return "On campus"
    def get_degree_level(self, response):
        if 'undergraduate' in response.url:
            return 'Undergraduate'
        else:
            return 'Postgraduate'
    def get_intake_month(self, response):
        return ''
    def get_city(self, response):
        return 'Edinburgh'
    def get_ielts(self, response):
        modules_element_ielts = ' '.join(response.xpath('//*[contains(text(), "IELTS")]/parent::*').extract())
        ielts_list = re.findall(r'(\d+\.\d+?)|\.\d+', (modules_element_ielts))
        ielts_list = list(dict.fromkeys(ielts_list))
        if ielts_list:
            ielts_length = len(ielts_list)
            if ielts_length == 1:
                return [ielts_list[0],'','','','']
            elif ielts_length == 2:
                return [ielts_list[0], ielts_list[1],ielts_list[1],ielts_list[1],ielts_list[1]]
            else:
                return ['','','','', '']
        return ['','','','', '']
    def get_toefl(self, response):
        toefl = response.xpath("//abbr[contains(text(), 'TOEFL')]/following-sibling::text()[1]").get()
        if toefl:
            toefl_list = re.findall(r'\b\d+\b', (toefl))
            toefl_length = len(toefl_list)
            if toefl_length == 1:
                return [toefl_list[0],'','','','']
            elif toefl_length == 2:
                return [toefl_list[0], toefl_list[1],toefl_list[1],toefl_list[1],toefl_list[1]]
            else:
                return ['','','','', '']
        return ['','','','', '']
    def get_english(self, response):
        english = response.xpath("//li[contains(text(), 'Cambridge English')]/text()").get()
        if not english:
            english = response.xpath("//abbr[contains(text(), 'CAE')]/following-sibling::text()[2]").get()
        if not english:
            english = response.xpath("//abbr[contains(text(), 'CPE')]/following-sibling::text()[1]").get()
        if english:
            english_list = re.findall(r'\b\d+\b', (english))
            english_length = len(english_list)
            if english_length == 1:
                return [english_list[0],'','','','']
            elif english_length == 2:
                return [english_list[0], english_list[1],english_list[1],english_list[1],english_list[1]]
            else:
                return ['','','','', '']
        return ['','','','', '']
    def get_overview(self, response):
        overview = response.xpath("//a[contains(text(), 'Programme description')]/parent::*/parent::*/following-sibling::*//p/text()").extract()

        if len(overview) == 0:
            overview = response.xpath("//div[@id='proxy_introduction']//p/text()").extract()
            if len(overview) >= 1:
                return overview[0]
        elif len(overview) >= 1:
            return overview[0]
        return ''
    
    def get_career(self, response):
        career = ' '.join(response.xpath('//h2[contains(text(), "Career outcomes")]/following-sibling::*/descendant::p[position()<=1]/text()').extract())
        if career:
            return career
        return ''
    def get_course_structure(self, response):
        course_desc = ','.join(response.xpath('//h3[contains(text(), "Modules")]/following-sibling::*/descendant::accordion//@name').extract())
        if course_desc:
            return course_desc
        course_desc = ','.join(response.xpath('//h4[contains(text(), "Modules include:")]/following-sibling::p[position()<=1]/text()').extract())
        if course_desc:
            return course_desc
        course_desc = ','.join(response.xpath('//h4[contains(text(), "Modules")]/following-sibling::p[position()<=1]/text()').extract())
        if course_desc:
            return course_desc
        course_desc = ','.join(response.xpath('//h4[contains(text(), "Modules")]/following-sibling::*/descendant::li/text()').extract())
        if course_desc:
            return course_desc

        course_desc = ','.join(response.xpath('//h4[contains(text(), "Modules")]/parent::*//p[position()<=2]/text()').extract())
        if course_desc:
            return course_desc
        course_desc = ','.join(response.xpath('//h2[contains(text(), "Course structure")]/parent::*//div/p[position()<=4]/text()').extract())
        if course_desc:
            return course_desc
        course_desc = ','.join(response.xpath('//h2[contains(text(), "Course structure")]/parent::*//ul/li[position()<=4]/text()').extract())
        if course_desc:
            return course_desc
        course_desc = ','.join(response.xpath('//strong[contains(text(), "modules")]/following-sibling::*/descendant::li/text()').extract())
        if course_desc:
            return course_desc
        return ''
    def get_study_load(self, response):
        duration_elements  = response.xpath('//li/strong[contains(text(), "Duration")]/parent::*/text()').extract()
        duration = duration_elements[1] if duration_elements else ''
        duration = duration.replace("–", "").strip()
        duration = duration.replace("One year", "1 year")
        duration = duration.replace("One Year", "1 year")
        duration = duration.replace("Two year", "2 years")
        duration = duration.replace("Two Year", "2 years")
        
        is_full = "full-time" in duration or "full time" in duration or "Full-time" in duration or "Full-time" in duration
        is_part = "part time" in duration or "Par time" in duration or "par time" in duration or "part-time" in duration or "Part time" in duration or "Part-time" in duration
        if is_full and is_part:
            return "Both"
        if is_part:
            return "Part time"
        if is_full:
            return "Full time"
        return "Full time"
    def get_fee_url(self, response):
        name = response.xpath("//h3[contains(text(), 'Tuition Fees')]//following-sibling::p//a//@href").get()
        return name
    
    def get_currency(self, text):
        currency = re.findall(r'[\£]\b\d+\b', (text.replace(',','')))
        if len(currency) >= 1:
            return currency[0].replace('£','')
        return ''
    def course_data_fee(self, response):
        fee_element = response.xpath("//td[contains(text(), '£')]/text()").extract()
       
        fee_list =[]
        for item in fee_element:
            fee_list.append(self.get_currency(item.strip()))
        fee_len = len(fee_list)
        dom_fee = ''
        int_fee = ''
        if fee_len == 3:
            dom_fee = fee_list[1]
            int_fee = fee_list[2]
        if fee_len == 2:
            dom_fee = fee_list[0]
            int_fee = fee_list[1]
        if fee_len == 1:
            dom_fee = fee_list[0]
            
        fee_year = response.xpath("//td[contains(text(), '2020')]/text()").get()
        if '2020/1' in fee_year:
            fee_year = "2021"
        if '2021/2' in fee_year:
            fee_year = "2022"
        if '2019/0' in fee_year:
            fee_year = "2020"
            
       
        course_data = response.meta
        self.course_name.append(course_data['name'])
        self.category.append(course_data['category'])
        self.course_website.append(course_data['url'])
        self.duration.append(course_data['duration'])
        self.duration_term.append(course_data['duration_term'])
        self.study_mode.append(course_data['study_mode'])
        self.study_load.append(course_data['study_load'])
        self.degree.append(course_data['degree_level'])
        self.intake_month.append(course_data['intake_month'])
        self.city.append(course_data['city'])
        self.fee_dom.append(dom_fee)
        self.fee_int.append(int_fee)
        self.fee_year.append(fee_year)
        self.fee_term.append('Year')
        self.currency.append("GBP")
        self.apply_month.append("")
        ielts_all = course_data['ielts']
        if not ielts_all[0] and not int_fee:
            self.dom_only.append("True")
        else:
            self.dom_only.append("False")
        
        self.ielts_overall.append(ielts_all[0])
        self.ielts_reading.append(ielts_all[1])
        self.ielts_writing.append(ielts_all[2])
        self.ielts_listening.append(ielts_all[3])
        self.ielts_speaking.append(ielts_all[4])

        toefl_all = course_data['toefl']
        self.toefl_overall.append(toefl_all[0])
        self.toefl_reading.append(toefl_all[1])
        self.toefl_writing.append(toefl_all[2])
        self.toefl_listening.append(toefl_all[3])
        self.toefl_speaking.append(toefl_all[4])

        self.pte_overall.append("")
        self.pte_reading.append("")
        self.pte_writing.append("")
        self.pte_listening.append("")
        self.pte_speaking.append("")

        
        self.course_struct.append(course_data['course_structure'])
        self.career.append(course_data['career'])
        self.course_des.append(course_data['overview'])
