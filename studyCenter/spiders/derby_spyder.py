
import scrapy
import json
from scrapy.http import JsonRequest
import time
from bs4 import BeautifulSoup
import os.path
import re
import numpy as np
import pandas as pd
import json


class DerbySpider(scrapy.Spider):
    name = "derby"
    file_name = 'Derby University'
    token = ""
    data_directory = ""
    course_name = []
    category = []
    course_website = []
    duration = []
    duration_term = []
    degree = []
    intake_month = []
    apply_month = []
    city = []
    study_mode = []
    dom_only = []
    prerequisites = []
    fee_dom = []
    fee_int = []
    fee_year = []
    fee_term = []
    currency = []
    study_load = []

    ielts_overall = []
    ielts_reading = []
    ielts_writing = []
    ielts_listening = []
    ielts_speaking = []
    toefl_overall = []
    toefl_reading = []
    toefl_writing = []
    toefl_listening = []
    toefl_speaking = []
    pte_overall = []
    pte_reading = []
    pte_writing = []
    pte_listening = []
    pte_speaking = []
    course_struct = []
    career = []
    course_des = []
    index = 1
    generatepptx = True

    def start_requests(self):
        x = range(1, 60)
        urls = ['https://www.derby.ac.uk/course-search/?query=%20&collection=uod-meta&start_rank=1&f.result%20type%7Ctype=courses&f.study%20option%7CstudyOption=Full-time&f.study%20option%7CstudyOption=Part-time&f.study%20option%7CstudyOption=Online']
        for n in x:
            url = "https://www.derby.ac.uk/course-search/?query=%20&collection=uod-meta&f.result%20type%7Ctype=courses&f.study%20option%7CstudyOption=Full-time&f.study%20option%7CstudyOption=Part-time&f.study%20option%7CstudyOption=Online&start_rank=" + \
                str((n*10) + 1)
            urls.append(url)
        for url in urls:
            yield scrapy.Request(url=url, callback=self.pare_course_list)

    def pare_course_list(self, response):
        # //section
        # /html/body/main/main/section[contains(!@class, "course-teaser")]
        all = response.xpath('//section[contains(@class, "course-teaser")]')
        for item in all:
            level = item.xpath(".//div[contains(@class,'course-teaser-course-level')]/text()").get()
            if 'Undergraduate' in level or 'Postgraduate' in level:
                #self.index = self.index + 1
                overview = item.xpath(".//div[contains(@class,'course-teaser-description')]/text()").get()
                name = item.xpath(".//div[contains(@class,'course-teaser-heading')]//a/text()").get()
                duration_element = item.xpath(
                    ".//div[contains(text(),'Study options')]/following-sibling::*/text()").get()
                duration = re.findall(r'[-+]?\d*\.\d+|\d+', duration_element)
                duration_term = ''
                if 'Year' in duration_element or 'year' in duration_element:
                    duration_term = 'Years'
                elif 'Month' in duration_element or 'month' in duration_element:
                    duration_term = "Months"
                elif 'Week' in duration_element or 'week' in duration_element:
                    duration_term = 'Weeks'
                study_load = ''
                if 'Full-time' in duration_element and 'Part-time' in duration_element:
                    study_load = 'Both'
                elif 'Full-time' in duration_element:
                    study_load = "Full time"
                elif 'Part-time' in duration_element:
                    study_load = 'Full time'
                tag_turquoise = item.xpath(".//div[contains(@class,'tag-turquoise')]/text()").get()
                study_mode = 'On campus'
                if tag_turquoise and 'Online' in tag_turquoise:
                    study_mode = 'Online'
                url = item.xpath(".//div[contains(@class,'course-teaser-heading')]//a//@href").get()
                if len(duration) == 0:
                    duration = ''
                else:
                    duration = duration[0]

                if 'Undergraduate' in level:
                    level = "Undergraduate"
                elif 'Postgraduate-time' in level:
                    level = "Postgraduate"
                meta_data = {
                    'level': level,
                    'duration': duration,
                    'duration_term': duration_term,
                    'overview': overview.strip(),
                    'title': name,
                    'study_mode': study_mode,
                    'study_load': study_load
                }
                yield scrapy.Request(url=url, callback=self.parese_course, meta=meta_data)

    def parese_course(self, response):
        basic_meta = response.meta
        if basic_meta:
            name = basic_meta['title']
            category = self.get_category(response)
            if category:
                # print(category)
                course_data = {
                    'name': name,
                    'category': category,
                    'url': self.get_url(response),
                    'duration': basic_meta['duration'],
                    'duration_term': basic_meta['duration_term'],
                    'study_mode': basic_meta['study_mode'],
                    'study_load': basic_meta['study_load'],
                    'degree_level': basic_meta['level'],
                    'intake_month': self.get_intake_month(response),
                    'city': self.get_city(response),
                    'ielts': self.get_ielts(response),
                    'toefl': self.get_toefl(response),
                    'english': self.get_english(response),
                    'overview': basic_meta['overview'],
                    'career': self.get_career(response),
                    'course_structure': self.get_course_structure(response),
                    'dom_fee': self.get_dom_fee(response),
                    'int_fee': self.get_int_fee(response),
                    'fee_term': self.get_fee_term(response),
                    'fee_year': self.get_fee_year(response),
                }
                print(self.get_english(response))
                if not self.get_english(response)[0]:
                    self.index = self.index + 1
                    print(response.url)
                self.set_data(course_data)

    def get_url(self, response):
        url = response.xpath('/html/head/link[@rel="canonical"]/@href').get()
        if url:
            return url
        return ''

    def set_data(self, course_data):
        self.course_name.append(course_data['name'])
        self.category.append(course_data['category'])
        self.course_website.append(course_data['url'])
        self.duration.append(course_data['duration'])
        self.duration_term.append(course_data['duration_term'])
        self.study_mode.append(course_data['study_mode'])
        self.study_load.append(course_data['study_load'])
        self.degree.append(course_data['degree_level'])
        self.intake_month.append(course_data['intake_month'])
        self.city.append(course_data['city'])
        self.fee_dom.append(course_data['dom_fee'])
        self.fee_int.append(course_data['int_fee'])
        self.fee_year.append(course_data['fee_year'])
        self.fee_term.append(course_data['fee_term'])
        self.currency.append("GBP")
        self.apply_month.append("")
        ielts_all = course_data['ielts']
        if not ielts_all[0] and not course_data['int_fee']:
            self.dom_only.append("True")
        else:
            self.dom_only.append("False")

        self.ielts_overall.append(ielts_all[0])
        self.ielts_reading.append(ielts_all[1])
        self.ielts_writing.append(ielts_all[2])
        self.ielts_listening.append(ielts_all[3])
        self.ielts_speaking.append(ielts_all[4])

        #toefl_all = course_data['toefl']
        self.toefl_overall.append('')
        self.toefl_reading.append('')
        self.toefl_writing.append('')
        self.toefl_listening.append('')
        self.toefl_speaking.append('')

        self.pte_overall.append("")
        self.pte_reading.append("")
        self.pte_writing.append("")
        self.pte_listening.append("")
        self.pte_speaking.append("")

        self.course_struct.append(course_data['course_structure'])
        self.career.append(course_data['career'])
        self.course_des.append(course_data['overview'])

    def get_name(self, response):
        name = response.xpath("//h1[contains(@class, 'page-header')]/text()").get()
        if name:
            return name.strip()
        return ''

    def get_fee_year(self, response):
        #fee_dom_2021 = response.xpath("//span[contains(text(), 'UK and EU 2020-21')]/following-sibling::*")
        fee_term = response.xpath('//h3[contains(text(), "2020/21 Fees")]').get()
        if fee_term:
            return "2021"
        return ''

    def get_fee_term(self, response):
        #fee_dom_2021 = response.xpath("//span[contains(text(), 'UK and EU 2020-21')]/following-sibling::*")
        fee_term = response.xpath(
            '//td/strong[contains(text(), "International")]/parent::*/following-sibling::*//p[contains(text(), "£")]/text()').get().strip()
        if fee_term:
            if 'full programme' in fee_term:
                return "Full"
            if 'year' in fee_term or 'Year' in fee_term:
                return "Year"
        return ''

    def get_dom_fee(self, response):
        #fee_dom_2021 = response.xpath("//span[contains(text(), 'UK and EU 2020-21')]/following-sibling::*")
        fee_dom_2021 = response.xpath(
            '//td/strong[contains(text(), "UK/EU")]/parent::*/following-sibling::*//p[contains(text(), "£")]/text()').get().strip()
        if fee_dom_2021 and ('year' in fee_dom_2021 or 'full programme' in fee_dom_2021):
            return self.get_currency(fee_dom_2021)
        return ''

    def get_int_fee(self, response):
        #fee_dom_2021 = response.xpath("//span[contains(text(), 'UK and EU 2020-21')]/following-sibling::*")
        fee_dom_2021 = response.xpath(
            '//td/strong[contains(text(), "International")]/parent::*/following-sibling::*//p[contains(text(), "£")]/text()').get().strip()
        if fee_dom_2021 and ('year' in fee_dom_2021 or 'full programme' in fee_dom_2021):
            return self.get_currency(fee_dom_2021)
        return ''

    def get_category(self, response):
        category = response.xpath('/html/head/meta[@name="subject"]/@content').get()
        if category:
            return category
        return ''

    def get_duration(self, response):
        duration_elements = response.xpath('//li/strong[contains(text(), "Duration")]/parent::*/text()').extract()
        duration = duration_elements[1] if duration_elements else ''
        duration = duration.replace("–", "").strip()
        duration = duration.replace("One year", "1 year")
        duration = duration.replace("One Year", "1 year")
        duration = duration.replace("Two year", "2 years")
        duration = duration.replace("Two Year", "2 years")
        duration_extract = re.findall(r'\b\d+\b', duration)
        duration_value = duration_extract[0] if duration_extract else ''
        return duration_value

    def get_duration_term(self, response):
        duration_elements = response.xpath('//li/strong[contains(text(), "Duration")]/parent::*/text()').extract()
        duration = duration_elements[1] if duration_elements else ''
        duration = duration.replace("–", "").strip()
        duration = duration.replace("One year", "1 year")
        duration = duration.replace("One Year", "1 year")
        duration = duration.replace("Two year", "2 years")
        duration = duration.replace("Two Year", "2 years")
        if 'Year' in duration or 'year' in duration:
            return "Years"
        elif 'Month' in duration or 'month' in duration:
            return "Months"
        elif 'Week' in duration or 'week' in duration:
            return "Weeks"
        else:
            return ""

    def get_study_mode(self, response):
        location = response.xpath(
            '//title[contains(text(), "Course location")]/text()/parent::*/parent::*/following-sibling::*/text()').get()
        if "Online" in location or "Offsite" in location:
            return "Online"
        else:
            return "On campus"

    def get_degree_level(self, response):
        if 'undergraduate' in response.url:
            return 'Undergraduate'
        else:
            return 'Postgraduate'

    def get_intake_month(self, response):
        month_element = response.xpath("//p[contains(text(), 'Start date')]/following-sibling::*/text()").get()
        if month_element:
            month_element = month_element.replace(', See below', '')
        return month_element

    def get_city(self, response):
        location = response.xpath('/html/head/meta[@name="location"]/@content').get()
        if location:
            return location.split(',')[0]
        return ''

    def get_ielts(self, response):
        modules_element_ielts = ' '.join(response.xpath(
            '//*[contains(text(), "IELTS")]/parent::*/following-sibling::*/text()').extract())
        if modules_element_ielts and '7.0 (in all areas, except 6.5 in written element)' in modules_element_ielts:
            return ['7.0', '7.0', '6.5', '7.0', '7.0']
        ielts_list = re.findall(r'(\d+\.\d+?)|\.\d+', (modules_element_ielts))
        ielts_list = list(dict.fromkeys(ielts_list))
        if len(ielts_list) == 0:
            modules_element_ielts = ' '.join(response.xpath(
                '//*[contains(text(), "IELTS")]/parent::*/text()').extract())
            ielts_list = re.findall(r'(\d+\.\d+?)|\.\d+', (modules_element_ielts))
            ielts_list = list(dict.fromkeys(ielts_list))
        if len(ielts_list) == 0:
            modules_element_ielts = ' '.join(response.xpath('//*[contains(text(), "IELTS")]/text()').extract())
            if modules_element_ielts and 'IELTS minimum overall: 6' in modules_element_ielts:
                return ['6.0', '6.0', '6.0', '6.0', '6.0']
            ielts_list = re.findall(r'(\d+\.\d+?)|\.\d+', (modules_element_ielts))
            ielts_list = list(dict.fromkeys(ielts_list))
        if ielts_list:
            ielts_length = len(ielts_list)
            if ielts_length == 1:
                return [ielts_list[0], '', '', '', '']
            elif ielts_length == 2:
                return [ielts_list[0], ielts_list[1], ielts_list[1], ielts_list[1], ielts_list[1]]
            else:
                return ['', '', '', '', '']
        return ['', '', '', '', '']

    def get_toefl(self, response):
        toefl = response.xpath("//abbr[contains(text(), 'TOEFL')]/following-sibling::text()[1]").get()
        if toefl:
            toefl_list = re.findall(r'\b\d+\b', (toefl))
            toefl_length = len(toefl_list)
            if toefl_length == 1:
                return [toefl_list[0], '', '', '', '']
            elif toefl_length == 2:
                return [toefl_list[0], toefl_list[1], toefl_list[1], toefl_list[1], toefl_list[1]]
            else:
                return ['', '', '', '', '']
        return ['', '', '', '', '']

    def get_english(self, response):
        english = response.xpath("//li[contains(text(), 'Cambridge English')]/text()").get()
        if not english:
            english = response.xpath("//abbr[contains(text(), 'CAE')]/following-sibling::text()[2]").get()
        if not english:
            english = response.xpath("//abbr[contains(text(), 'CPE')]/following-sibling::text()[1]").get()
        if english:
            english_list = re.findall(r'\b\d+\b', (english))
            english_length = len(english_list)
            if english_length == 1:
                return [english_list[0], '', '', '', '']
            elif english_length == 2:
                return [english_list[0], english_list[1], english_list[1], english_list[1], english_list[1]]
            else:
                return ['', '', '', '', '']
        return ['', '', '', '', '']

    def get_overview(self, response):
        overview = response.xpath(
            "//a[contains(text(), 'Programme description')]/parent::*/parent::*/following-sibling::*//p/text()").extract()

        if len(overview) == 0:
            overview = response.xpath("//div[@id='proxy_introduction']//p/text()").extract()
            if len(overview) >= 1:
                return overview[0]
        elif len(overview) >= 1:
            return overview[0]
        return ''

    def get_career(self, response):
        career = ' '.join(response.xpath(
            '//h3[contains(text(), "Careers")]/following-sibling::p[position()<=1]/text()').extract())
        if career:
            return career
        career = ' '.join(response.xpath(
            '//h2[contains(text(), "Careers")]/parent::*/following-sibling::*//p[position()<=1]/text()').extract())
        if career:
            return career
        return ''

    def get_course_structure(self, response):
        # module-list-menu-item-details-label
        course_desc = ','.join(response.xpath(
            '//div[contains(@class, "module-list-menu-item-details-label")]/text()').extract())
        if course_desc:
            return course_desc
        course_desc = ','.join(response.xpath(
            '//p[contains(text(), "You’ll study modules such as:")]/following-sibling::*//li/text()').extract())
        if course_desc:
            return course_desc
        return ''

    def get_study_load(self, response):
        duration_elements = response.xpath('//li/strong[contains(text(), "Duration")]/parent::*/text()').extract()
        duration = duration_elements[1] if duration_elements else ''
        duration = duration.replace("–", "").strip()
        duration = duration.replace("One year", "1 year")
        duration = duration.replace("One Year", "1 year")
        duration = duration.replace("Two year", "2 years")
        duration = duration.replace("Two Year", "2 years")

        is_full = "full-time" in duration or "full time" in duration or "Full-time" in duration or "Full-time" in duration
        is_part = "part time" in duration or "Par time" in duration or "par time" in duration or "part-time" in duration or "Part time" in duration or "Part-time" in duration
        if is_full and is_part:
            return "Both"
        if is_part:
            return "Part time"
        if is_full:
            return "Full time"
        return "Full time"

    def get_fee_url(self, response):
        name = response.xpath("//h3[contains(text(), 'Tuition Fees')]//following-sibling::p//a//@href").get()
        return name

    def get_currency(self, text):
        currency = re.findall(r'[\£]\b\d+\b', (text.replace(',', '')))
        if len(currency) >= 1:
            return currency[0].replace('£', '')
        return ''

    def course_data_fee(self, response):
        fee_element = response.xpath("//td[contains(text(), '£')]/text()").extract()

        fee_list = []
        for item in fee_element:
            fee_list.append(self.get_currency(item.strip()))
        fee_len = len(fee_list)
        dom_fee = ''
        int_fee = ''
        if fee_len == 3:
            dom_fee = fee_list[1]
            int_fee = fee_list[2]
        if fee_len == 2:
            dom_fee = fee_list[0]
            int_fee = fee_list[1]
        if fee_len == 1:
            dom_fee = fee_list[0]

        fee_year = response.xpath("//td[contains(text(), '2020')]/text()").get()
        if '2020/1' in fee_year:
            fee_year = "2021"
        if '2021/2' in fee_year:
            fee_year = "2022"
        if '2019/0' in fee_year:
            fee_year = "2020"

        course_data = response.meta
        self.course_name.append(course_data['name'])
        self.category.append(course_data['category'])
        self.course_website.append(course_data['url'])
        self.duration.append(course_data['duration'])
        self.duration_term.append(course_data['duration_term'])
        self.study_mode.append(course_data['study_mode'])
        self.study_load.append(course_data['study_load'])
        self.degree.append(course_data['degree_level'])
        self.intake_month.append(course_data['intake_month'])
        self.city.append(course_data['city'])
        self.fee_dom.append(dom_fee)
        self.fee_int.append(int_fee)
        self.fee_year.append(fee_year)
        self.fee_term.append('Year')
        self.currency.append("GBP")
        self.apply_month.append("")
        ielts_all = course_data['ielts']
        if not ielts_all[0] and not int_fee:
            self.dom_only.append("True")
        else:
            self.dom_only.append("False")

        self.ielts_overall.append(ielts_all[0])
        self.ielts_reading.append(ielts_all[1])
        self.ielts_writing.append(ielts_all[2])
        self.ielts_listening.append(ielts_all[3])
        self.ielts_speaking.append(ielts_all[4])

        toefl_all = course_data['toefl']
        self.toefl_overall.append(toefl_all[0])
        self.toefl_reading.append(toefl_all[1])
        self.toefl_writing.append(toefl_all[2])
        self.toefl_listening.append(toefl_all[3])
        self.toefl_speaking.append(toefl_all[4])

        self.pte_overall.append("")
        self.pte_reading.append("")
        self.pte_writing.append("")
        self.pte_listening.append("")
        self.pte_speaking.append("")

        self.course_struct.append(course_data['course_structure'])
        self.career.append(course_data['career'])
        self.course_des.append(course_data['overview'])
