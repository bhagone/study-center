
import scrapy
import json
from scrapy.http import JsonRequest
import time
from bs4 import BeautifulSoup
import os.path
import re
import numpy as np
import pandas as pd


class EdgehillSpider(scrapy.Spider):
    name = "edgehill"
    token = ""
    data_directory = ""
    course_name = []
    category = []
    course_website = []
    duration = []
    duration_term = []
    degree = []
    intake_month = []
    apply_month = []
    city = []
    study_mode = []
    dom_only = []
    prerequisites = []
    fee_dom = []
    fee_int = []
    fee_year = []
    fee_term = []
    currency = []
    study_load = []

    ielts_overall = []
    ielts_reading = []
    ielts_writing = []
    ielts_listening = []
    ielts_speaking = []
    toefl_overall = []
    toefl_reading = []
    toefl_writing = []
    toefl_listening = []
    toefl_speaking = []
    pte_overall = []
    pte_reading = []
    pte_writing = []
    pte_listening = []
    pte_speaking = []
    course_struct = []
    career = []
    course_des = []

    def start_requests_old(self):
        current_directory = os.getcwd()
        self.data_directory = os.path.join(current_directory, r'data_JCU')
        if not os.path.exists(self.data_directory):
            os.makedirs(self.data_directory)

        if 1 == 0:
            x = range(0, 29)
            urls = []
            for n in x:
                url = "https://www.yorksj.ac.uk/courses/?hideTabs=true&tab=courses&profile=_default&sort=metacourseTitle&collection=yorksj-meta&query=!showall&start_rank=" + str((n*10) + 1)
                urls.append(url)
            for url in urls:
                yield scrapy.Request(url=url, callback=self.parse_courses_metadata)
        else:
            urls = [
                "https://www.edgehill.ac.uk/courses/child-and-adolescent-mental-health-and-wellbeing/tab/entry-criteria/"
            ]
            for url in urls:
                yield scrapy.Request(url=url, callback=self.parse_courses)

    def start_requests(self):
        urls = [
            "https://www.edgehill.ac.uk/study/undergraduate/",
            "https://www.edgehill.ac.uk/study/postgraduate/",
            # "https://www.edgehill.ac.uk/courses/mres/tab/finance/"
            # "https://www.edgehill.ac.uk/courses/sports-coaching/tab/entry-criteria/",
            # "https://www.edgehill.ac.uk/courses/mathematics/",
            # "https://www.edgehill.ac.uk/courses/applied-management-of-offending-behaviour/tab/finance/"
            # "https://www.edgehill.ac.uk/courses/geology-with-geography/"
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse_courses_first_label)

    def parse_courses_first_label(self, response):
        all = response.xpath('//a//@href').getall()
        all_fill = list(filter(lambda x: len(x) > 50, all))
        filter_all = list(filter(lambda x: 'https://www.edgehill.ac.uk/study/undergraduate/' in x, all_fill))
        filter_all_post = list(filter(lambda x: 'https://www.edgehill.ac.uk/study/postgraduate/' in x, all_fill))
        for url in filter_all:
            yield scrapy.Request(url=url, callback=self.parse_courses_metadata)
        for url in filter_all_post:
            yield scrapy.Request(url=url, callback=self.parse_courses_metadata)
        # custom-link

    def parse_courses_metadata(self, response):
        #all_ss = response.xpath('//a[contains(@class, "course-title")]/a/text()')
        all = response.xpath('//a//@href').getall()
        filter_all = list(filter(lambda x: 'https://www.edgehill.ac.uk/courses/' in x, all))
        for url in filter_all:
            yield scrapy.Request(url=url, callback=self.parse_courses)

    def parse_courses(self, response):
        # body_soup = BeautifulSoup(response.text, 'html.parser')
        int_studet_apply = 'International Students Can Apply' in response.text
        if 'International Students Can Apply' in response.text:
            print('************************************************************************')
            print(int_studet_apply)
            print(response.url)
        # //p[contains(@class, "h-alt")]/text()
        name = response.xpath('//h1[contains(@class, "course-title")]/a/text()').get().strip()
        overview = ' '.join(response.xpath('//h3[contains(text(), "Overview")]/following-sibling::*/descendant::p/text()').extract())
        # category
        cat_elements = ' '.join(response.xpath('//*[@id="overview"]/descendant::th[contains(text(), "Subjects:")]/following-sibling::*/a/text()').extract())
        if cat_elements:
            cat_elements = cat_elements.strip()

        duration_elements = ' '.join(response.xpath('//*[@id="overview"]/descendant::th[contains(text(), "Length:")]/following-sibling::*/text()').extract())
        duration = duration_elements if duration_elements else ''
        duration = duration.replace("–", "").strip()
        duration = duration.replace("One year", "1 year")
        duration = duration.replace("One Year", "1 year")
        duration = duration.replace("Two year", "2 years")
        duration = duration.replace("Two Year", "2 years")
        duration_extract = re.findall(r'\b\d+\b', duration)
        duration_value = duration_extract[0] if duration_extract else ''
        try:
            duration_value_second = duration_extract[1] if duration_extract else ''
        except:
            duration_value_second = ''
        study_load = ""
        duration_term = ''
        if "year" in duration or "years" in duration or "Year" in duration or "Years" in duration:
            duration_term = "Years"
        if "month" in duration or "month" in duration or "months" in duration or "Months" in duration:
            duration_term = "Months"
        elif "Weeks" in duration:
            duration_term = "Weeks"
        is_full = "full-time" in duration or "full time" in duration or "Full-time" in duration or "Full Time" in duration or "Full-Time" in duration or "Full-time" in duration
        is_part = "part time" in duration or "part-time" in duration or "Part time" in duration or "Part Time" in duration or "Part-Time" in duration or "Part-time" in duration
        if is_full and is_part:
            study_load = "Both"
        if is_part:
            study_load = "Part time"
        if is_full:
            study_load = "Full time"

        cate = response.xpath('//li/strong[contains(text(), "School")]/parent::*/text()').extract()
        category_name = cate[1] if cate else ''
        category_name = category_name.replace("–", "").strip()
        international_fee = ''
        home_fee = ''
        fee_elements = ' '.join(response.xpath('//*[@id="finance"]/descendant::h4[contains(text(), "Tuition Fees")]/following-sibling::*/text()').extract())
        if '£' not in fee_elements:
            fee_elements = ' '.join(response.xpath('//*[@id="finance"]/descendant::h4[contains(text(), "Tuition Fees")]/following-sibling::ul').extract())
            if '2021/22' in fee_elements:
                fee_elements = ' '.join(response.xpath('//*[@id="finance"]/descendant::h4[contains(text(), "Tuition Fees")]/following-sibling::ul/li[contains(text(), "2021/22")]').extract())
            elif '2020/21' in fee_elements:
                fee_elements = ' '.join(response.xpath('//*[@id="finance"]/descendant::h4[contains(text(), "Tuition Fees")]/following-sibling::ul/li[contains(text(), "2020/21")]').extract())
        home_fee_list = re.findall(r'[\$¢£]\b\d+\b', (fee_elements.strip().replace(',', '')))
        try:
            home_fee = home_fee_list[0] if home_fee_list else ''
            international_fee = home_fee_list[1] if home_fee_list else ''
        except:
            home_fee = ""
            international_fee = ""
        fee_term = ''
        if '2020/21' in fee_elements:
            fee_term = "2021"
        elif fee_elements and '2020/21' in fee_elements:
            fee_term = "2021"
        elif '2019/20' in fee_elements:
            fee_term = "2020"
        elif fee_elements and '2019/20' in fee_elements:
            fee_term = "2020"
        elif fee_elements and '2021/22' in fee_elements:
            fee_term = "2022"
        location_elements = ' '.join(response.xpath('//*[@id="overview"]/descendant::th[contains(text(), "Location")]/following-sibling::*/text()').extract())
        location = location_elements if location_elements else ''
        study_mode = ""

        if "Online" in location or "Offsite" in location:
            study_mode = "Online"
        else:
            study_mode = "On campus"

        course = ' '.join(response.xpath('//*[@id="finance"]/p/text()').extract())
        print('55555555555555555555555555555555')
        print(course)
        #response.xpath('//p[contains(@class, "h-alt")]/text()').get().strip()
        if "Postgraduate" in course or "postgraduate" in course:
            course = "Postgraduate"
        elif "Undergraduate" in course or "undergraduate" in course:
            course = "Undergraduate"
        else:
            course = ''
        print('55555555555555555555555555555555')
        print(course)
        # Start Month
        intake_month = ''
        start_month = ' '.join(response.xpath('//*[@id="overview"]/div[1]/descendant::th[contains(text(), "Dates")]/following-sibling::*/text()').extract())
        if start_month:
            intake_month = ','.join(np.unique(re.findall(r'(?:Janary|February|Marcg|April|May|June|July|Augest|September|October|November|December)', (start_month))))
        # start module
        course_desc = ' '.join(response.xpath('//h3[contains(text(), "Modules")]/following-sibling::*/descendant::p/text()').extract())

        # start IELTS
        modules_element_ielts = ' '.join(response.xpath('//p[contains(text(), "IELTS")]/text()').extract())

        ielts_list = re.findall(r'(\d+\.\d+?)|\.\d+', (modules_element_ielts))
        ielts_list = list(dict.fromkeys(ielts_list))

        if len(ielts_list) == 0:
            modules_element_ielts = ' '.join(response.xpath('//p[contains(text(), "IELTS")]/text()').extract())
            ielts_list = re.findall(r'\b\d+\b', (modules_element_ielts))
        if len(ielts_list) == 0:
            ielts_list = ["", ""]

        career_outcomes = ' '.join(response.xpath('//h3[contains(text(), "Career Prospects")]/following-sibling::p/text()').extract())
        self.course_name.append(name)
        self.category.append(cat_elements)
        self.course_website.append(response.url)
        self.duration_term.append(duration_term)
        if duration_term == 'Weeks':
            if duration_value_second:
                self.duration.append(duration_value_second)
            else:
                self.duration.append(duration_value)

        self.city.append(location)
        self.degree.append(course)
        self.intake_month.append(intake_month)
        self.apply_month.append("")

        self.study_mode.append(study_mode)
        dom_fee = ''
        if home_fee.replace('£', ''):
            dom_fee = int(home_fee.replace('£', ''))
            if dom_fee < 2000:
                dom_fee = ''
        int_fee = ''
        if international_fee.replace('£', ''):
            int_fee = int(international_fee.replace('£', ''))
            if int_fee < 4000:
                int_fee = ''

        self.fee_dom.append(dom_fee)
        self.fee_int.append(int_fee)

        self.fee_year.append("Year")
        self.fee_term.append(fee_term)
        self.currency.append("GBP")
        self.study_load.append(study_load)
        over_all = ielts_list[0] if ielts_list else ''
        ielts_len = len(ielts_list)
        if (not over_all and not international_fee and "International students" not in duration) or int_studet_apply:
            self.dom_only.append("True")
        else:
            self.dom_only.append("False")
        if ielts_len == 1:
            self.ielts_overall.append(over_all)
            self.ielts_reading.append("")
            self.ielts_writing.append("")
            self.ielts_listening.append("")
            self.ielts_speaking.append("")
        elif ielts_len == 2:
            ielts_other = ielts_list[1] if ielts_list else ''
            self.ielts_overall.append(over_all)
            self.ielts_reading.append(ielts_other)
            self.ielts_writing.append(ielts_other)
            self.ielts_listening.append(ielts_other)
            self.ielts_speaking.append(ielts_other)
        else:
            self.ielts_overall.append("")
            self.ielts_reading.append("")
            self.ielts_writing.append("")
            self.ielts_listening.append("")
            self.ielts_speaking.append("")

        self.toefl_overall.append("")
        self.toefl_reading.append("")
        self.toefl_writing.append("")
        self.toefl_listening.append("")
        self.toefl_speaking.append("")

        self.pte_overall.append("")
        self.pte_reading.append("")
        self.pte_writing.append("")
        self.pte_listening.append("")
        self.pte_speaking.append("")

        self.course_struct.append(course_desc)
        self.career.append(career_outcomes)
        self.course_des.append(overview)
