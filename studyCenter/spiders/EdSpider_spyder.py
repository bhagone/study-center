
import scrapy
import json
from scrapy.http import JsonRequest
import time
from bs4 import BeautifulSoup
import os.path
import re
import numpy as np
import pandas as pd


class EdSpider(scrapy.Spider):
    name = "ed"
    file_name = 'The University of Edinburgh'
    token = ""
    data_directory = ""
    course_name = []
    category = []
    course_website = []
    duration = []
    duration_term = []
    degree = []
    intake_month = []
    apply_month = []
    city = []
    study_mode = []
    dom_only = []
    prerequisites = []
    fee_dom = []
    fee_int = []
    fee_year = []
    fee_term = []
    currency = []
    study_load = []

    ielts_overall = []
    ielts_reading = []
    ielts_writing = []
    ielts_listening = []
    ielts_speaking = []
    toefl_overall = []
    toefl_reading = []
    toefl_writing = []
    toefl_listening = []
    toefl_speaking = []
    pte_overall = []
    pte_reading = []
    pte_writing = []
    pte_listening = []
    pte_speaking = []
    course_struct = []
    career = []
    course_des = []

    def start_requests(self):
        if 1 == 1:
            urls = [
                # "https://www.ed.ac.uk/studying/postgraduate/degrees/index.php&edition=2020?r=site%2Fsearch&pgSearch=&yt0=&moa=a",
                "https://www.ed.ac.uk/studying/undergraduate/degrees/index.php?action=degreeList"
            ]
            for url in urls:
                yield scrapy.Request(url=url, callback=self.pare_course_list)
        else:
            urls = [
                "https://www.ed.ac.uk/studying/undergraduate/degrees/index.php?action=programme&code=RL42",
                "https://www.ed.ac.uk/studying/postgraduate/degrees/index.php?r=site/view&edition=2020&id=910"
                # "https://www.ed.ac.uk/studying/undergraduate/degrees/index.php?action=degreeList"
            ]
            for url in urls:
                yield scrapy.Request(url=url, callback=self.parese_course)

    def pare_course_list(self, response):
        all = response.xpath('//div//a[contains(@class, "list-group-item")]')
        for item in all:
            url = item.xpath(".//@href").get()
            url = "https://www.ed.ac.uk" + url
            yield scrapy.Request(url=url, callback=self.parese_course)
        # custom-link

    def get_name(self, response):
        name = response.xpath("//h1[contains(@class, 'page-header')]/text()").get()
        if name:
            return name.strip()
        return ''

    def get_category(self, response):
        category = response.xpath("//span[contains(text(), 'School:')]/following-sibling::text()[1]").get()
        if category:
            return category.strip()
        return ''

    def get_duration(self, response):
        duration = response.xpath("//span[contains(text(), 'Duration:')]/following-sibling::text()[1]").get()
        if not duration:
            duration = ''
        duration = re.findall(r'\b\d+\b', duration)
        try:
            return duration[0]
        except:
            return ''

    def get_duration_term(self, response):
        duration = response.xpath("//span[contains(text(), 'Duration:')]/following-sibling::text()[1]").get()
        if not duration:
            duration = ''
        if 'Year' in duration or 'year' in duration:
            return "Years"
        elif 'Month' in duration or 'month' in duration:
            return "Months"
        elif 'Week' in duration or 'week' in duration:
            return "Weeks"
        else:
            return ""

    def get_study_mode(self, response):
        is_phone = 'glyphicon-phone' in response.text
        if is_phone:
            return "Online"
        else:
            return "On campus"

    def get_degree_level(self, response):
        if 'undergraduate' in response.url:
            return 'Undergraduate'
        else:
            return 'Postgraduate'

    def get_intake_month(self, response):
        return ''

    def get_city(self, response):
        return 'Edinburgh'

    def get_ielts(self, response):
        ielts = response.xpath("//abbr[contains(text(), 'IELTS')]/following-sibling::text()[1]").get()
        if ielts:
            ielts_list = re.findall(r'(\d+\.\d+?)|\.\d+', (ielts))
            ielts_length = len(ielts_list)
            if ielts_length == 1:
                return [ielts_list[0], '', '', '', '']
            elif ielts_length == 2:
                return [ielts_list[0], ielts_list[1], ielts_list[1], ielts_list[1], ielts_list[1]]
            else:
                return ['', '', '', '', '']
        return ['', '', '', '', '']

    def get_toefl(self, response):
        toefl = response.xpath("//abbr[contains(text(), 'TOEFL')]/following-sibling::text()[1]").get()
        if toefl:
            toefl_list = re.findall(r'\b\d+\b', (toefl))
            toefl_length = len(toefl_list)
            if toefl_length == 1:
                return [toefl_list[0], '', '', '', '']
            elif toefl_length == 2:
                return [toefl_list[0], toefl_list[1], toefl_list[1], toefl_list[1], toefl_list[1]]
            else:
                return ['', '', '', '', '']
        return ['', '', '', '', '']

    def get_english(self, response):
        english = response.xpath("//li[contains(text(), 'Cambridge English')]/text()").get()
        if not english:
            english = response.xpath("//abbr[contains(text(), 'CAE')]/following-sibling::text()[2]").get()
        if not english:
            english = response.xpath("//abbr[contains(text(), 'CPE')]/following-sibling::text()[1]").get()
        if english:
            english_list = re.findall(r'\b\d+\b', (english))
            english_length = len(english_list)
            if english_length == 1:
                return [english_list[0], '', '', '', '']
            elif english_length == 2:
                return [english_list[0], english_list[1], english_list[1], english_list[1], english_list[1]]
            else:
                return ['', '', '', '', '']
        return ['', '', '', '', '']

    def get_overview(self, response):
        overview = response.xpath("//a[contains(text(), 'Programme description')]/parent::*/parent::*/following-sibling::*//p/text()").extract()

        if len(overview) == 0:
            overview = response.xpath("//div[@id='proxy_introduction']//p/text()").extract()
            if len(overview) >= 1:
                return overview[0]
        elif len(overview) >= 1:
            return overview[0]
        return ''

    def get_career(self, response):
        career = response.xpath("//a[contains(text(), 'Career opportunities')]/parent::*/parent::*/following-sibling::*//p/text()").extract()

        if len(career) == 0:
            career = response.xpath("//div[@id='proxy_collapsecareer_opp']//li/text()").extract()
            if len(career) >= 1:
                return ' '.join(career)
        elif len(career) >= 1:
            return career[0]
        return ''

    def get_course_structure(self, response):
        structure = response.xpath("//a[contains(text(), 'What you will study')]/parent::*/parent::*/following-sibling::*//p/text()").extract()

        if len(structure) == 0:
            structure = response.xpath("//div[@id='proxy_collapsehow_taught']//p/text()").extract()
            if len(structure) >= 1:
                return structure[0]
        elif len(structure) >= 1:
            return structure[0]
        return ''

    def get_study_load(self, response):
        study_load = response.xpath("//span[contains(text(), 'Delivery:')]/following-sibling::text()[1]").get()
        if not study_load:  # Study modes
            study_load = response.xpath("//span[contains(text(), 'Study modes')]/following-sibling::text()[1]").get()
        if not study_load:  # Study modes
            study_load = ''
        if "Full-time" in study_load and "Part-time" in study_load:
            return "Both"
        elif "Full-time" in study_load or "full-time" in study_load:
            return "Full time"
        elif "Part-time" in study_load or "part-time" in study_load:
            return "Part time"
        else:
            return ''

    def get_fee_url(self, response):
        name = response.xpath("//h3[contains(text(), 'Tuition Fees')]//following-sibling::p//a//@href").get()
        return name

    def parese_course(self, response):
        name = self.get_name(response)
        category = self.get_category(response)
        course_data = {
            'name': name,
            'category': category,
            'url': response.url,
            'duration': self.get_duration(response),
            'duration_term': self.get_duration_term(response),
            'study_mode': self.get_study_mode(response),
            'study_load': self.get_study_load(response),
            'degree_level': self.get_degree_level(response),
            'intake_month': self.get_intake_month(response),
            'city': self.get_city(response),
            'ielts': self.get_ielts(response),
            'toefl': self.get_toefl(response),
            'english': self.get_english(response),
            'overview': self.get_overview(response),
            'career': self.get_career(response),
            'course_structure': self.get_course_structure(response)
        }
        fee_url = self.get_fee_url(response)
        if fee_url:
            yield scrapy.Request(url=fee_url, callback=self.course_data_fee, meta=course_data)

    def get_currency(self, text):
        currency = re.findall(r'[\£]\b\d+\b', (text.replace(',', '')))
        if len(currency) >= 1:
            return currency[0].replace('£', '')
        return ''

    def course_data_fee(self, response):
        fee_element = response.xpath("//td[contains(text(), '£')]/text()").extract()

        fee_list = []
        for item in fee_element:
            fee_list.append(self.get_currency(item.strip()))
        fee_len = len(fee_list)
        dom_fee = ''
        int_fee = ''
        if fee_len == 3:
            dom_fee = fee_list[1]
            int_fee = fee_list[2]
        if fee_len == 2:
            dom_fee = fee_list[0]
            int_fee = fee_list[1]
        if fee_len == 1:
            dom_fee = fee_list[0]

        fee_year = response.xpath("//td[contains(text(), '2020')]/text()").get()
        if '2020/1' in fee_year:
            fee_year = "2021"
        if '2021/2' in fee_year:
            fee_year = "2022"
        if '2019/0' in fee_year:
            fee_year = "2020"

        course_data = response.meta
        self.course_name.append(course_data['name'])
        self.category.append(course_data['category'])
        self.course_website.append(course_data['url'])
        self.duration.append(course_data['duration'])
        self.duration_term.append(course_data['duration_term'])
        self.study_mode.append(course_data['study_mode'])
        self.study_load.append(course_data['study_load'])
        self.degree.append(course_data['degree_level'])
        self.intake_month.append(course_data['intake_month'])
        self.city.append(course_data['city'])
        self.fee_dom.append(dom_fee)
        self.fee_int.append(int_fee)
        self.fee_year.append(fee_year)
        self.fee_term.append('Year')
        self.currency.append("GBP")
        self.apply_month.append("")
        ielts_all = course_data['ielts']
        if not ielts_all[0] and not int_fee:
            self.dom_only.append("True")
        else:
            self.dom_only.append("False")

        self.ielts_overall.append(ielts_all[0])
        self.ielts_reading.append(ielts_all[1])
        self.ielts_writing.append(ielts_all[2])
        self.ielts_listening.append(ielts_all[3])
        self.ielts_speaking.append(ielts_all[4])

        toefl_all = course_data['toefl']
        self.toefl_overall.append(toefl_all[0])
        self.toefl_reading.append(toefl_all[1])
        self.toefl_writing.append(toefl_all[2])
        self.toefl_listening.append(toefl_all[3])
        self.toefl_speaking.append(toefl_all[4])

        self.pte_overall.append("")
        self.pte_reading.append("")
        self.pte_writing.append("")
        self.pte_listening.append("")
        self.pte_speaking.append("")

        self.course_struct.append(course_data['course_structure'])
        self.career.append(course_data['career'])
        self.course_des.append(course_data['overview'])
