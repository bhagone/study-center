
import scrapy
import json
from scrapy.http import JsonRequest
import time
from bs4 import BeautifulSoup
import os.path
import re
import numpy as np
import pandas as pd
import json

class HartpurySpider(scrapy.Spider):
    name = "hartpury"
    token = ""
    data_directory = ""
    course_name = []
    category = []
    course_website = []
    duration = []
    duration_term = []
    degree = []
    intake_month = []
    apply_month = []
    city = []
    study_mode = []
    dom_only = []
    prerequisites = []
    fee_dom = []
    fee_int = []
    fee_year = []
    fee_term = []
    currency = []
    study_load = []

    ielts_overall = []
    ielts_reading = []
    ielts_writing = []
    ielts_listening = []
    ielts_speaking = []
    toefl_overall = []
    toefl_reading = []
    toefl_writing = []
    toefl_listening = []
    toefl_speaking = []
    pte_overall = []
    pte_reading = []
    pte_writing = []
    pte_listening = []
    pte_speaking = []
    course_struct = []
    career = []
    course_des = []
    def start_requests(self):
        current_directory = os.getcwd()
        self.data_directory = os.path.join(current_directory, r'data_JCU')
        if not os.path.exists(self.data_directory):
            os.makedirs(self.data_directory)
        
        if 1 == 1: 
            x = range(1,9)
            urls = []
            for n in x:
                url = "https://www.hartpury.ac.uk/courses/?search=&searchType=courses&searchWithin=&qualification=postgraduate%2cundergraduate&page=" + str(n)
                urls.append(url)
            for url in urls:
                yield scrapy.Request(url=url, callback=self.parse_courses_first_label)
        else:
            urls = [
                "https://www.edgehill.ac.uk/courses/child-and-adolescent-mental-health-and-wellbeing/tab/entry-criteria/"
            ]
            for url in urls:
                yield scrapy.Request(url=url, callback=self.parse_courses_first_label)
    def start_requests_old(self):
        urls = [
            #"https://www.edgehill.ac.uk/study/undergraduate/",
            #"https://www.edgehill.ac.uk/study/postgraduate/",
            #"https://www.edgehill.ac.uk/courses/mres/tab/finance/"
            #"https://www.edgehill.ac.uk/courses/sports-coaching/tab/entry-criteria/",
            #"https://www.edgehill.ac.uk/courses/mathematics/",
            #"https://www.edgehill.ac.uk/courses/applied-management-of-offending-behaviour/tab/finance/"
            "https://www.hartpury.ac.uk/university/courses/undergraduate/bsc-hons-strength-and-conditioning/"
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse_courses)
    def parse_courses_first_label(self, response):
        all = response.xpath('//td')
        for td in all:
            a = td.xpath(".//a//@href")
            if len(a) == 1:
                url = a.get()
                parent = td.xpath(".//a/parent::*/following-sibling::td/text()").extract()
                subject = parent[0]
                level = parent[1]
                duration = parent[2]
                meta =dict(
                    duration=duration,
                    subject=subject,
                    level=level,
                )
                yield scrapy.Request(url='https://www.hartpury.ac.uk' + url, callback=self.parse_courses, meta=meta)
    def parse_courses_metadata(self, response):
        all = response.xpath('//a//@href').getall()
        filter_all = list(filter(lambda x: 'https://www.edgehill.ac.uk/courses/' in x, all))
        for url in filter_all:
            yield scrapy.Request(url=url, callback=self.parse_courses)
    
    def parse_courses(self, response):
        meta = response.meta
        subject = meta['subject']
        level = meta['level']
        duration = meta['duration']

        name = response.xpath('//h1[contains(@class, "course__name")]/text()').get().strip()
        overview = response.xpath('//div[contains(@class, "rte__content")]//p/text()').get()
        if not overview:
            overview = response.xpath('//div[contains(@class, "rte__content")]//strong/text()').get()
            if not overview:
                print("****************************************")
                print(response.url)
        elif "This course has 100% student satisfaction" in overview:
            overview = response.xpath('//div[contains(@class, "rte__content")]//p[1]s/text()').get()

        #return
        #category
        cat_elements  =subject

        duration_elements  = duration
        duration = duration_elements if duration_elements else ''
        duration = duration.replace("–", "").strip()
        duration = duration.replace("One year", "1 year")
        duration = duration.replace("One Year", "1 year")
        duration = duration.replace("Two year", "2 years")
        duration = duration.replace("Two Year", "2 years")
        duration_extract = re.findall(r'\b\d+\b',duration)
        duration_value = duration_extract[0] if duration_extract else ''
        study_load = ""
        duration_term = ''
        if "year" in duration or "years" in duration or "Year" in duration or "Years" in duration:
            duration_term = "Years"
        if "month" in duration or "month" in duration or "months" in duration or "Months" in duration:
            duration_term = "Months"
        elif "Weeks" in duration:
            duration_term = "Weeks"
        is_full = "full-time" in duration or "full time" in duration or "Full-time" in duration or "Full-time" in duration
        is_part = "part time" in duration or "part-time" in duration or "Part time" in duration or "Part-time" in duration
        if is_full and is_part:
            study_load = "Both"
        if is_part:
            study_load = "Part time"
        if is_full:
            study_load = "Full time"

      
        if level == 'Postgraduate':
            home_fee = '7000'
            international_fee = '13000'
        elif level == 'Undergraduate':
            home_fee = '9250'
            international_fee = '13000'
        fee_lement = response.xpath('//h2[contains(text(), "Tuition Fees")]/following-sibling::table/tbody/tr[2]')
        study_year = ''
        if fee_lement:
            study_year = fee_lement.xpath(".//td/text()")[0].get()
            study_load = fee_lement.xpath(".//td/text()")[1].get()
            if fee_lement.xpath(".//td/text()")[2].get():
                home_fee = fee_lement.xpath(".//td/text()")[2].get().replace(",", "").replace("£", '')
            if fee_lement.xpath(".//td/text()")[3].get():
                international_fee = fee_lement.xpath(".//td/text()")[3].get().replace(",", "").replace("£", '')

        location_elements  = ' '.join(response.xpath('//*[@id="overview"]/descendant::th[contains(text(), "Location")]/following-sibling::*/text()').extract())
        location = location_elements if location_elements else ''
        study_mode = "On campus"
        
        if "Online" in location or "Offsite" in location:
            study_mode = "Online"
        else:
            study_mode = "On campus"
       
        course =  level
        # Start Month
        intake_month = ''
        start_month  = ' '.join(response.xpath('//*[@id="overview"]/div[1]/descendant::th[contains(text(), "Dates")]/following-sibling::*/text()').extract())
        if start_month:
            intake_month = ','.join(np.unique(re.findall(r'(?:Janary|February|Marcg|April|May|June|July|Augest|September|October|November|December)', (start_month))))
        #start module
        course_desc =  ' '.join(response.xpath('//h2[contains(text(), "Learning and assessment")]/following-sibling::p[position()<2]/text()').extract())

        #start IELTS
        modules_element_ielts = ' '.join(response.xpath('//p[contains(text(), "IELTS")]/text()').extract())

        ielts_list = re.findall(r'(\d+\.\d+?)|\.\d+', (modules_element_ielts))
        ielts_list = list(dict.fromkeys(ielts_list))

        if len(ielts_list) == 0:
            modules_element_ielts = ' '.join(response.xpath('//p[contains(text(), "IELTS")]/text()').extract())
            ielts_list = re.findall(r'\b\d+\b', (modules_element_ielts))
        if len(ielts_list) == 0:
            ielts_list= []
        
        career_outcomes = ' '.join(response.xpath('//h2[contains(text(), "Industry opportunities")]/following-sibling::p[position()<2]/text()').extract())
        
        self.course_name.append(name)
        self.category.append(cat_elements)
        self.course_website.append(response.url)
        self.duration.append(duration_value)
        self.duration_term.append(duration_term)

        self.city.append("Gloucester")
        self.degree.append(course)
        self.intake_month.append(intake_month)
        self.apply_month.append("")

        self.study_mode.append(study_mode)
        
        self.fee_dom.append(home_fee.replace('£',''))
        self.fee_int.append(international_fee.replace('£',''))

        self.fee_year.append("2020")
        self.fee_term.append("Year")
        self.currency.append("GBP")
        self.study_load.append("Full time")
        over_all = ielts_list[0] if ielts_list else ''
        ielts_len = len(ielts_list)
        if not over_all and not international_fee and "International students" not in duration:
            self.dom_only.append("True")
        else:
            self.dom_only.append("False")
        if ielts_len == 1:
            self.ielts_overall.append(over_all)
            self.ielts_reading.append("")
            self.ielts_writing.append("")
            self.ielts_listening.append("")
            self.ielts_speaking.append("")
        elif ielts_len == 2:
            ielts_other = ielts_list[1] if ielts_list else ''
            self.ielts_overall.append(over_all)
            self.ielts_reading.append(ielts_other)
            self.ielts_writing.append(ielts_other)
            self.ielts_listening.append(ielts_other)
            self.ielts_speaking.append(ielts_other)
        else:
            self.ielts_overall.append("")
            self.ielts_reading.append("")
            self.ielts_writing.append("")
            self.ielts_listening.append("")
            self.ielts_speaking.append("")

        self.toefl_overall.append("")
        self.toefl_reading.append("")
        self.toefl_writing.append("")
        self.toefl_listening.append("")
        self.toefl_speaking.append("")

        self.pte_overall.append("")
        self.pte_reading.append("")
        self.pte_writing.append("")
        self.pte_listening.append("")
        self.pte_speaking.append("")

        
        self.course_struct.append(course_desc)
        self.career.append(career_outcomes)
        self.course_des.append(overview)
        print("****************************************")
        print("OverView")
        print(overview)
        print("Career")
        print(career_outcomes)
        print("course_desc")
        print(course_desc)

